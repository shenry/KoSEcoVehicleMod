# Changes

Vehicle Capacity Increased

Powered vehicles have better speeds on roads vs terrain:

Values can be seen [here](https://docs.google.com/spreadsheets/d/13elxsA-qFMCoBPx7DctdflcACfG9ZS71O244BMqAAUg/edit?usp=sharing)

# Use

Use `make help` to see commands.
`make all` To clean, make, and pack.

To install the game, copy the `Mod` folder in the zip and paste it into your `Eco_Data/Server` folder to overwrite the needed files.

# About

Made by Sydney (FSMimp). Values from KatherineOfSky Discord Eco Moderators.

This is a work in progress, which is why commands are not consistent.
That is also why sed and perl are used, and sometimes and find unnecessarily.

Special thanks to Wally1169 for the spreadsheet, ground work and research.

https://gitlab.com/shenry/KoSEcoResearchMod


# Versions

v0.1.x - KoS Server 5
