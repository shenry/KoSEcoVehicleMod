MODNAME=KoSEcoVehicleMod
VERSION=v0.1.3

MODS=Mods
OBJECTS=$(MODS)/Objects
AUTOGEN=$(MODS)/AutoGen
VEHICLE=$(AUTOGEN)/Vehicle
BASEDIR=$(CURDIR)
SERVERDIR=$(BASEDIR)/Server
BACKUPDIR=$(BASEDIR)/ServerBak
README=$(BASEDIR)/README.md
CHANGES=$(BASEDIR)/CHANGES.diff
LICENSE=$(BASEDIR)/LICENSE.md

SKIDSTEER=$(OBJECTS)/SkidSteer.cs
EXCAVATOR=$(OBJECTS)/Excavator.cs

SERVERVEHICLE=$(SERVERDIR)/$(VEHICLE)
PACKAGE=$(BASEDIR)/$(MODNAME)_$(VERSION).zip

PACKEXTRAFILES=$(README) $(CHANGES) $(LICENSE)
PACKOBJECTS=$(SKIDSTEER) $(EXCAVATOR)
PACKVEHICLES=$(VEHICLE)/*.cs

# VEHICLES
# VEHICLES=SmallWoodCart WoodCart PoweredCart Truck SkidSteer Excavator
COMPONENTSTORAGE=this.GetComponent<PublicStorageComponent>().Initialize
COMPONENTVEHICLE=this.GetComponent<VehicleComponent>().Initialize
COMPONENTTOOL=this\.GetComponent<VehicleToolComponent>\(\)\.Initialize

# CAPACITY
# in grams
CAPACITY_SmallWoodCart=3600000
CAPACITY_WoodCart=7200000
CAPACITY_PoweredCart=12000000
CAPACITY_Truck=15000000
CAPACITY_SkidSteer=
CAPACITY_Excavator=

# SLOTS
SLOTS_SmallWoodCart=6
SLOTS_WoodCart=12
SLOTS_PoweredCart=20
SLOTS_Truck=25
SLOTS_SkidSteer=4
SLOTS_Excavator=10

# VEHICLE COMPONETS
# (maxSpeed, defaultSpeedEff, dict, seats)
COMPONET_PoweredCart=(20, .8f, roadEfficiency, 1)
COMPONET_Truck=(20, .8f, roadEfficiency, 2)

# VEHICLE SPEED
OLDDIRT={ typeof(DirtRoadBlock), 0.8f }, { typeof(DirtRoadWorldObjectBlock), 0.8f },
NEWDIRT={ typeof(DirtRoadBlock), 1f }, { typeof(DirtRoadWorldObjectBlock), 1f },

diff=diff --strip-trailing-cr

help:
	@echo 'Makefile for Eco Mod                                                      '
	@echo '                                                                          '
	@echo 'Usage:                                                                    '
	@echo '   make help               this display                                   '
	@echo '   make clean              remove the generated files and copy over backup'
	@echo '   make pack               generate the zip                               '
	@echo '   make mod                generate the mod files                         '
	@echo '   make info               display select information from files          '
	@echo '   make diff               display diff of tech files                     '
	@echo '   make all                run make clean, mod, and pack                  '
	@echo '                                                                          '

clean:
	$(RM) -r $(SERVERDIR)
	$(RM) $(MODNAME)_v*.zip
	$(RM) $(CHANGES)
	mkdir -p $(SERVERVEHICLE)
	mkdir -p $(SERVERDIR)/$(OBJECTS)
	cp -r $(BACKUPDIR)/$(VEHICLE)/*.cs $(SERVERVEHICLE)
	cp $(BACKUPDIR)/$(SKIDSTEER) $(BACKUPDIR)/$(EXCAVATOR) $(SERVERDIR)/$(OBJECTS)

diff:
	echo "# Automatically created by makefile" > $(CHANGES)
	$(diff) $(SERVERDIR)/$(VEHICLE) $(BACKUPDIR)/$(VEHICLE) >> $(CHANGES) || exit 0
	echo "$(diff) $(SERVERDIR)/$(EXCAVATOR) $(BACKUPDIR)/$(EXCAVATOR)" >> $(CHANGES)
	$(diff) $(SERVERDIR)/$(EXCAVATOR) $(BACKUPDIR)/$(EXCAVATOR) >> $(CHANGES) || exit 0
	echo "$(diff) $(SERVERDIR)/$(SKIDSTEER) $(BACKUPDIR)/$(SKIDSTEER)">> $(CHANGES)
	$(diff) $(SERVERDIR)/$(SKIDSTEER) $(BACKUPDIR)/$(SKIDSTEER) >> $(CHANGES) || exit 0
	cat $(CHANGES)

pack: diff
	(cd $(SERVERDIR); zip $(PACKAGE) $(PACKVEHICLES) $(PACKOBJECTS) ;)
	zip -j $(PACKAGE) $(PACKEXTRAFILES)

mod:
	echo "Edit Vehicle Capacity and slots"
	find $(SERVERVEHICLE) -type f -name "SmallWoodCart.cs" -print0 | xargs -0 sed -i''  -e 's/$(COMPONENTSTORAGE).*;/$(COMPONENTSTORAGE)($(SLOTS_SmallWoodCart), $(CAPACITY_SmallWoodCart));/g'
	find $(SERVERVEHICLE) -type f -name "WoodCart.cs" -print0 | xargs -0 sed -i''  -e 's/$(COMPONENTSTORAGE).*;/$(COMPONENTSTORAGE)($(SLOTS_WoodCart), $(CAPACITY_WoodCart));/g'
	find $(SERVERVEHICLE) -type f -name "PoweredCart.cs" -print0 | xargs -0 sed -i''  -e 's/$(COMPONENTSTORAGE).*;/$(COMPONENTSTORAGE)($(SLOTS_PoweredCart), $(CAPACITY_PoweredCart));/g'
	find $(SERVERVEHICLE) -type f -name "Truck.cs" -print0 | xargs -0 sed -i''  -e 's/$(COMPONENTSTORAGE).*;/$(COMPONENTSTORAGE)($(SLOTS_Truck), $(CAPACITY_Truck));/g'
	find $(SERVERDIR)/$(OBJECTS) -type f -name "SkidSteer.cs" -print0 | xargs -0 perl -pi -e 's/$(COMPONENTTOOL)\(([0-9]+), (.*)/$(COMPONENTTOOL)($(SLOTS_SkidSteer), \2/g'
	find $(SERVERDIR)/$(OBJECTS) -type f -name "Excavator.cs" -print0 | xargs -0 perl -pi -e 's/$(COMPONENTTOOL)\(([0-9]+), (.*)/$(COMPONENTTOOL)($(SLOTS_Excavator), \2/g'
	echo "Edit Vehicle Speeds"
	find $(SERVERVEHICLE) -type f -name "PoweredCart.cs" -print0 | xargs -0 sed -i''  -e 's/$(COMPONENTVEHICLE).*;/$(COMPONENTVEHICLE)$(COMPONET_PoweredCart);/g'
	find $(SERVERVEHICLE) -type f -name "Truck.cs" -print0 | xargs -0 sed -i''  -e 's/$(COMPONENTVEHICLE).*;/$(COMPONENTVEHICLE)$(COMPONET_Truck);/g'
	find $(SERVERVEHICLE) -type f -name "PoweredCart.cs" -print0 | xargs -0 sed -i''  -e 's/$(OLDDIRT)/$(NEWDIRT)/g'
	find $(SERVERVEHICLE) -type f -name "Truck.cs" -print0 | xargs -0 sed -i''  -e 's/$(OLDDIRT)/$(NEWDIRT)/g'

info:
	find $(SERVERVEHICLE) -type f -name "*.cs" -print0 | xargs -0 grep -nP 'this.GetComponent<PublicStorageComponent>()' | perl -pe 's/.*\d+\s*,\d+.*/Capacity: \2\t Slots: \1/gm' 

all: clean mod pack
